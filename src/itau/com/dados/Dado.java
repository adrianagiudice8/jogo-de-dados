package itau.com.dados;

public class Dado {

	public Integer sortearDado(Integer faces) {
			
		Double valor = Math.ceil(Math.random() * faces);
		int valorInteiro = valor.intValue();

		return valorInteiro;
	
		
	}
}
