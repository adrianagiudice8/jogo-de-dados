package itau.com.dados;


public class Jogada {

	public static Integer[] jogadaUnica(Integer numeroFaces) {
		Integer[] jogadas = new Integer[4];

		Dado dado = new Dado();
		int total = 0;

		for (int i = 0; i < 3; i++) {
			jogadas[i] = dado.sortearDado(numeroFaces);
			total += jogadas[i];
		}

		jogadas[3] = total;

		return jogadas;

	}

	public static String[] jogadas(Integer numeroJogadas, Integer numeroFaces) {

		String[] jogadas = new String[numeroJogadas];
		Integer[] jogada;
		String texto = "";

		for (int i = 0; i < numeroJogadas; i++) {

			jogada = Jogada.jogadaUnica(numeroFaces);

			for (int c = 0; c < jogada.length; c++) {
				if (c == jogada.length - 1)
					texto += String.valueOf(jogada[c]);
				else
					texto += String.valueOf(jogada[c]) + ", ";

			}

			jogadas[i] = texto;
			texto = "";
		}

		return jogadas;

	}

}
